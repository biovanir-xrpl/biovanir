# BioVANIR #



### What is BioVANIR? ###

**BioVANIR** (**V**erifiable **A**ccess to **N**FTs **I**nstilled with **R**eality) is a collection of 
**NFT**s (**N**on-**f**ungible **t**okens) focusing on biological research in the form of 
text, images, audio, video, research documents, scientific posters, or 3D models.